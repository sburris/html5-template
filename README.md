# About
A good starting point for a responsive web document:
* `index.html` has all of the important bits, and not much extra.
* `style.css` has sane CSS defaults (only a few rules to fix the default eyesore)

Also see the `sass` branch if you are looking to use [Sass](https://sass-lang.com).

## Usage
Either copy the contents of `index.html`, or if you want to use the whole structure:

1. Clone this repo:
```
git clone git@gitlab.com:quaternion-inst/html5-template.git my-project
cd my-project
```
2. Add your own repo as upstream:
```
git remote rename origin old-origin
git remote add origin git@gitlab.com:username/reponame.git
```
1. Edit `README.md` for your project
2. Uncomment the `<meta>` line and enter a description for `content` that will appear in search results.
3. Uncomment the `<link>` line when you add a stylesheet.

### Sass
To switch the the `sass` branch:
```
git switch sass
```

Add your own Sass in `sass/style.sass` and then compile it using the included `Makefile` by running `make` (requires `sassc` to be installed).
